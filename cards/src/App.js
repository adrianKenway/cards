import React, { Component } from 'react';
import "./App.css";
import firebase from "firebase";

var config = {
    apiKey: "AIzaSyBaBQToMXY4nBPHePM3Stg3q2rbgKoUfxY",
    authDomain: "maisvacina.firebaseapp.com",
    databaseURL: "https://maisvacina.firebaseio.com",
    projectId: "maisvacina",
    storageBucket: "maisvacina.appspot.com",
    messagingSenderId: "77982322258"
  };
firebase.initializeApp(config);

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      email : "",
      senha:"",
      emailValido:"",
      senhaValido:"",
      usuario:""
    }
    this.dataBase = firebase.database();
    this.atualizaEmail = this.atualizaEmail.bind(this)
    this.atualizaSenha = this.atualizaSenha.bind(this)
    this.enviar = this.enviar.bind(this)
    this.atualizaUsuario = this.atualizaUsuario.bind(this)
    // eslint-disable-next-line
    this.email = firebase.database().ref('users/' + "Adriano" + "/email");
    this.email.on('value',(snapshot) =>
      this.setState({emailValido:snapshot.val()})
    );
    // eslint-disable-next-line
    this.senha = firebase.database().ref('users/' + "Adriano" + "/senha");
    this.senha.on('value',(snapshot) =>
      this.setState({senhaValido:snapshot.val()})
    );
  }

  atualizaEmail(e){
    this.setState({email:e.target.value})
  }


  atualizaSenha(e){
    this.setState({senha:e.target.value})
  }
  enviar(){
  // eslint-disable-next-line
  firebase.database().ref('users/' + this.state.usuario).set({
    email:this.state.email,
    senha:this.state.senha
  });
  }

  atualizaUsuario(e){
    this.setState({usuario:e.target.value})
  }

  render() {
    return (
      <div className="App">
      <form>
      <div className="form-group row">
        <label  className="col-sm-2 col-form-label">Usuário</label>
        <div className="col-sm-10">
          <input type="text" className="form-control-plaintext" onChange={this.atualizaUsuario} id="staticEmail" value={this.state.usuario}/>
        </div>
      </div>
      <div className="form-group row">
        <label  className="col-sm-2 col-form-label">Email</label>
        <div className="col-sm-10">
          <input type="text" className="form-control-plaintext" onChange={this.atualizaEmail} id="staticEmail" value={this.state.email}/>
        </div>
      </div>
      <div className="form-group row">
        <label className="col-sm-2 col-form-label">Password</label>
        <div className="col-sm-10">
          <input onChange={this.atualizaSenha} value={this.state.senha} type="password" className="form-control" id="inputPassword" placeholder="Password"/>
        </div>
      </div>
      <button onClick={this.enviar} className="btn btn-button btn-success">Enviar</button>
    </form>
      </div>
    );
  }
}

export default App;
